﻿using System;
using System.Collections.Generic;
namespace Service{
    public class EDIFACTService{
        public string[] segregate(string input,string key){
            string[] firtsCut=input.Replace("\r\n","").Split('\'');
            List<string> onlyLocs=new List<string>();
            for(int findLoc=0;findLoc<firtsCut.Length;findLoc++){
                if(firtsCut[findLoc].StartsWith(key)){
                    onlyLocs.Add(firtsCut[findLoc]);
                }
            }
            int toReturnCount=0;
            string[] toReturn=new string[onlyLocs.Count*2];
            for(int iLocs=0;iLocs<onlyLocs.Count;iLocs++,toReturnCount++){
                string[] tempLocs=onlyLocs[iLocs].Split('+');
                toReturn[toReturnCount]=tempLocs[1];
                toReturnCount++;
                toReturn[toReturnCount]=tempLocs[2];
            }
            return toReturn;
        }
    }
}
